# Title 1

## Snippet

```html
<h1 class="text-base font-semibold leading-7"></h1>
```

## Source

- [Left-aligned](https://tailwindui.com/components/application-ui/data-display/description-lists) snippet by Tailwind Labs
