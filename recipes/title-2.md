# Title 2

## Snippet

```html
<h1 class="text-base font-bold leading-7"></h1>
```

### Subtitle

```html
<h1 class="text-base font-medium"></h1>
```
