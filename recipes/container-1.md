# Container 1

## Snippet

```html
<div class="mx-auto max-w-screen-sm px-5"></div>
```

```html
<div class="mx-auto max-w-screen-md px-5"></div>
```

```html
<div class="mx-auto max-w-screen-lg px-5"></div>
```

## Notes

- Apply to the `<body>` element if possible.

## Source

- [Astro Nano](https://github.com/markhorn-dev/astro-nano) by [Mark Horn](https://github.com/markhorn-dev)
  - https://github.com/markhorn-dev/astro-nano/blob/v1.0.0/src/components/Container.astro
- [Astro Sphere](https://github.com/markhorn-dev/astro-sphere) by [Mark Horn](https://github.com/markhorn-dev)
  - https://github.com/markhorn-dev/astro-sphere/blob/v1.0.1/src/components/Container.astro
- [HyperUI](https://www.hyperui.dev/) by [Mark Mead](https://github.com/markmead)
  - https://www.hyperui.dev/blog/how-to-write-better-containers-in-tailwindcss
- [Max-Width](https://tailwindcss.com/docs/max-width) documentation
